import isAuthenticated from "./authHelper"

const authVerify = () => {
  let exp = isAuthenticated().exp * 1000

  if (Date.now() >= exp) {
    sessionStorage.removeItem("token");
    alert("Your token has expired and you will now be logged out. Please log back in again")
    return false
  } else {
    return true
  }

}

export default authVerify