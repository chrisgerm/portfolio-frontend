import { Container } from "react-bootstrap";

const Footer = () => {
  return (
    <Container className="text-center">
      <footer className="text-center">
        <p>&copy; Chris Germishuys. All Rights Reserved.</p>
        <p>
          <a href="#privacyPolicy">Privacy Policy</a>
        </p>
        <p>
          <a href="#termsAndConditions">Terms and Conditions</a>
        </p>
      </footer>
    </Container>
  );
};

export default Footer;
