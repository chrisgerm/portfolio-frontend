import { Navbar, Nav, Container, NavDropdown } from "react-bootstrap";
import { useHistory } from "react-router-dom";


const Navigation = (props) => {
  let history = useHistory();

  // Logout function to remove token and redirect to login page
  const logout = (event) => {
    event.preventDefault();
    sessionStorage.removeItem("token");
    props.setAuth(false);
    history.push("/login");
  };

  return (
    <Navbar collapseOnSelect expand="lg">
      <Container>
        <Navbar.Brand href="/">
          <img
            alt="Logo for Portfolio Site"
            src="/images/cg-logo.svg"
            width="50"
            height="50"
            className="d-inline-block align-top"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Item>
              <Nav.Link href="/">Home</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="/resume">Resume</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="/portfolio">Portfolio</Nav.Link>
            </Nav.Item>
            {/* The following will render the menu items based on whether the user is logged in or not */}
            {!props.auth ? (<Nav.Item><Nav.Link href="/contact">Contact</Nav.Link></Nav.Item>) : ("")}
            {!props.auth ? ("") : <NavDropdown title="Administration">
              <NavDropdown.Item href="/admin/user">Add User</NavDropdown.Item>
              <NavDropdown.Item href="/admin/submissions">Manage Submissions</NavDropdown.Item>
              <NavDropdown.Item href="/admin/resume">Manage Resume Page</NavDropdown.Item>
              <NavDropdown.Item href="/admin/portfolio">Manage Portfolio Page</NavDropdown.Item>  
            </NavDropdown>}
            {!props.auth ? (<Nav.Item><Nav.Link href="/login">Login</Nav.Link></Nav.Item>) : (<Nav.Item><Nav.Link href="/logout" onClick={logout}>Logout</Nav.Link></Nav.Item>)}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Navigation;