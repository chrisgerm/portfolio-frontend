import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";

const Portfolio = () => {
  const [portfolioEntry, setPortfolio] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const response = await fetch(process.env.REACT_APP_API + `/portfolio`, {
        method: "GET",
      });
      const data = await response.json();
      setPortfolio(data);
    };
    getData();
  }, []);

  return (
    <Container className="p-5">
      <h1 className="text-center">Portfolio</h1>
      {portfolioEntry.map((portfolio) => (
        <article className="text-left" key={portfolio.id}>
          <h3>{portfolio.title}</h3>
          <p>{portfolio.content}</p>
          <p>
            <a href={portfolio.url} target="_blank" rel="noreferrer">
              {portfolio.title}
            </a>
          </p>
        </article>
      ))}
    </Container>
  );
};

export default Portfolio;
