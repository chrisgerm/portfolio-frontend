import { useEffect, useState } from "react";
import { Button, Container, Table } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import parseJwt from "../../helpers/authHelper";

const AdminResume = () => {
  const token = sessionStorage.getItem("token");
  const user = parseJwt(token).username;
  const [resumes, setResumes] = useState([]);
  const history = useHistory()

  const getData = async () => {
    const response = await fetch(
      process.env.REACT_APP_API + `/admin/resume`,
      {
        'method': "GET",
        'headers': {
          'Authorization': `Bearer ${token}`,
        },
      }
    );
    const data = await response.json();
    setResumes(data);
  };

  // eslint-disable-next-line
  useEffect(() => {getData()}, [token]);

  const editResume = (event, resume) => {
    event.preventDefault();
    let path = `/admin/resume/edit/${resume.id}`
    history.push(path, resume)
  }

  const addResume = (event, resume) => {
    event.preventDefault();
    let path = `/admin/resume/add`
    history.push(path, resume)
  }

  const deleteResume = async (event, resume) => {
    event.preventDefault();
    await fetch(process.env.REACT_APP_API `/admin/resume/delete/${resume.id}`, {
      'method': 'DELETE',
      'headers': {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
    getData()
  }

  return (
    <Container className="p-5">
      <h2 className="text-center">Resume Administration Panel</h2>
      <p>You are logged in as: {user}</p>
      <p>Use this page to modify and add entries on the resume page.</p>
      <Table className="my-2" striped bordered hover responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Title</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {resumes.length === 0 && (
            <tr>
              <td colSpan="4" className="text-center">
                <i>No entries found</i>
              </td>
            </tr>
          )}
          {resumes.length > 0 &&
            resumes.map((resume) => (
              <tr key={resume.id}>
                <td>{resume.id}</td>
                <td>{resume.title}</td>
                <td>{resume.content}</td>
                <td className="text-center"><Button variant="outline-primary" onClick={(e) => editResume(e, resume)}>Edit</Button></td>
                <td className="text-center"><Button variant="outline-danger" onClick={(e) => deleteResume(e, resume)}>Delete</Button></td>
              </tr>
            ))}
        </tbody>
      </Table>
      <Button variant="outline-primary" onClick={(e) => addResume(e, resumes)}>Add Entry</Button>
    </Container>
  );
};

export default AdminResume;
