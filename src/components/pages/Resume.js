import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";

const Resume = () => {
  const [resumeEntry, setResumeEntry] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const response = await fetch(process.env.REACT_APP_API + `/resume`, {
        method: "GET",
      });
      const data = await response.json();
      setResumeEntry(data);
    };
    getData();
  }, []);

  return (
    <Container className="p-5">
      <h1 className="text-center">Resume</h1>
      {resumeEntry.map((resume) => (
        <article className="text-left" key={resume.id}>
          <h3>{resume.title}</h3>
          <pre>{resume.content}</pre>
        </article>
      ))}
    </Container>
  );
};

export default Resume;
