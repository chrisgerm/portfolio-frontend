import { Container } from "react-bootstrap";

const Home = () => {
  return (
    <Container className="p-5">
      <main>
        <h1 className="text-center" >Welcome</h1>
        <p>
          My name is Chris Germishuys. I was born in South Africa and moved to
          Canada in 2016.
        </p>
        <p>
          I've always been interested in Technology and started my life on
          computers at the young age of 3. I enjoy playing games a lot. I've
          dabbled quite a bit in software development but I would like to
          further develop my skills by taking the Full Stack Web Development
          Course at York University.
        </p>
      </main>
    </Container>
  );
};

export default Home;
