import { useState } from "react";
import { Container, Button, Form, Col } from "react-bootstrap";


const Contact = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [content, setContent] = useState("");
  const [message, setMessage] = useState("");

  // This function submits the contact form to the backend and returns either an error on a field being incorrect or succcess if form is accepted.
  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(process.env.REACT_APP_API + `/contact_form/entries`, {
      method: "POST",
      headers: {
        'Accept': "application/json",
        'Content-Type': "application/json",
      },
      body: JSON.stringify({ name, email, phoneNumber, content }),
    });
    const payload = await response.json();
    if (response.status >= 400) {
      setMessage(
        `Please fix the following input fields to submit the form: ${payload.invalid.join(
          ", "
        )}`
      );
    } else {
      setMessage(`Congratulations! You have submitted the contact form. Please keep this id for your records: ${payload.id}`);
      setName("")
      setEmail("")
      setPhoneNumber("")
      setContent("")
    }
  };

  return (
    <Container className="p-5">
      <Form onSubmit={formSubmit}>
        <h1 className="text-center">Contact</h1>
        <p className="text-center">Please use this page to contact me and I will get back to you as soon as possible.</p>
        <p className="text-center"><strong>{message}</strong></p> {/* This will display the message based on the response from the backend. Would like to improve this in future to be per field and highlight correctly */}
        <Form.Group className="mb-3">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            name="email"
            id="emailEntry"
            value={email}           
            required
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Enter email address..."
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Phone Number</Form.Label>
          <Form.Control
            type="tel"
            name="phone"
            id="phoneEntry"
            value={phoneNumber}
            required
            onChange={(e) => setPhoneNumber(e.target.value)}
            placeholder="Enter phone number..."
            pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
          />
          <Form.Text className="text-muted">
            Please enter your number in the following format: 555-123-1234.
          </Form.Text>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Full Name</Form.Label>
          <Form.Control
            type="name"
            name="name"
            id="nameEntry"
            value={name}
            required
            onChange={(e) => setName(e.target.value)}
            placeholder="Enter full name..."
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Message</Form.Label>
          <Form.Control
            type="textarea"
            name="text"
            id="messageEntry"
            value={content}
            required
            onChange={(e) => setContent(e.target.value)}
            as="textarea"
            rows={3}
            placeholder="Please enter a message..."
          />
        </Form.Group>
        <Form.Group>
          <Col>
            <Button color="success" type="submit">
              Submit
            </Button>
          </Col>
        </Form.Group>
      </Form>
    </Container>
  );
};

export default Contact;
