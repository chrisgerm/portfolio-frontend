import { Button, Container, Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";

const ViewSubmission = (props) => {
  const history = useHistory();
  let entry = props.location.state;

  const backButton = () => {
    history.push('/admin/submissions')
  }

  return (
    <Container className="p-5">
      <h2 className="text-center">View Contact Form Entry</h2>
      <Form>
        <Form.Label>Name</Form.Label>
        <InputGroup className="mb-3">
          <Form.Control type="text" name="title" defaultValue={entry.name} disabled={true} />
        </InputGroup>
        <Form.Label>Email</Form.Label>
        <InputGroup className="mb-3">
          <Form.Control type="text" name="title" defaultValue={entry.email} disabled={true}/>
        </InputGroup>
        <Form.Label>Phone Number</Form.Label>
        <InputGroup className="mb-3">
          <Form.Control type="text" name="title" defaultValue={entry.phonenumber} disabled={true}/>
        </InputGroup>
        <Form.Label>Content</Form.Label>
        <InputGroup className="mb-3">
          <Form.Control type="text" name="content" as="textarea" rows={10} defaultValue={entry.content} disabled={true}/>
        </InputGroup>
      </Form>
      <Button variant="outline-primary" onClick={backButton}>Back</Button>
    </Container>
  );
};

export default ViewSubmission;
