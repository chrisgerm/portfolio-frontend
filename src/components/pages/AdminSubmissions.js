import { useEffect, useState } from "react";
import { Container, Table, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import parseJwt from "../../helpers/authHelper";

const AdminSubmissions = () => {
  const token = sessionStorage.getItem("token");
  const user = parseJwt(token).username;
  const [entries, setEntries] = useState([]);
  const history = useHistory();

  // useEffect hook is used here to fetch the contact form entries and display it on the page
  // Passing in the second argument of token ensures that the useEffect hook is not run infinitely and will only do a check once to see if the token value is changed and display the entries
  const getData = async () => {
    const response = await fetch(
      process.env.REACT_APP_API + `/contact_form/entries`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const data = await response.json();
    setEntries(data);
  };
  
  // eslint-disable-next-line
  useEffect(() => {getData();}, [token]);

  const viewSubmission = (event, entries) => {
    event.preventDefault();
    let path = `/admin/submissions/view/${entries.id}`
    history.push(path, entries)
  }

  const deleteSubmission = async (event, entries) => {
    event.preventDefault();
    await fetch(process.env.REACT_APP_API + `/contact_form/entries/delete/${entries.id}`, {
      'method': 'DELETE',
      'headers': {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
    getData()
  }

  return (
    <Container className="p-5">
      <h2 className="text-center">Contact Form Submissions</h2>
      <p>You are logged in as: {user}</p>
      <p>The following people have sent in feedback and/or requests.</p>
      <Table className="my-2" striped bordered hover responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Message</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {entries.length === 0 && (
            <tr>
              <td colSpan="4" className="text-center">
                <i>No entries found</i>
              </td>
            </tr>
          )}
          {entries.length > 0 &&
            entries.map((entry) => (
              <tr key={entry.id}>
                <td>{entry.id}</td>
                <td>{entry.name}</td>
                <td>{entry.phonenumber}</td>
                <td>{entry.email}</td>
                <td>{entry.content}</td>
                <td className="text-center"><Button variant="outline-primary" onClick={(e) => viewSubmission(e, entry)}>View</Button></td>
                <td className="text-center"><Button variant="outline-danger" onClick={(e) => deleteSubmission(e, entry)}>Delete</Button></td>
              </tr>
            ))}
        </tbody>
      </Table>
    </Container>
  );
};

export default AdminSubmissions;
