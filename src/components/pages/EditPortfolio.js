import { useState } from "react";
import { Button, Container, Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";

const EditPortfolio = (props) => {
  const token = sessionStorage.getItem("token");
  let id = props.match.params.id;
  let editPortfolio = props.location.state;
  const history = useHistory()
  const [message, setMessage] = useState("");
  const [portfolio, setPortfolio] = useState(editPortfolio);

  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(process.env.REACT_APP_API + `/admin/portfolio/edit/${id}`, {
      method: "PATCH",
      headers: {
        'Accept': "application/json",
        'Content-Type': "application/json",
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(portfolio),
    });
    const payload = await response.json();
    if (response.status >= 400) {
      setMessage(
        `Please fix the following input fields to submit the form: ${payload}
        )}`
      );
    } else {
      history.push("/admin/portfolio")
    }
  };


  const handleChange = (event) => {
    event.persist();
    setPortfolio((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };


  return (
    <Container className="p-5">
      <h2 className="text-center">Use this page to modify the portfolio entry.</h2>
      <p className="text-center"><strong>{message}</strong></p> {/* This will display the message based on the response from the backend. Would like to improve this in future to be per field and highlight correctly */}
      <Form onSubmit={(e) => formSubmit(e)}>
      <Form.Label>Title</Form.Label>
        <InputGroup className="mb-3">
          
          <Form.Control type="text" name="title" defaultValue={editPortfolio.title} onChange={handleChange} />
        </InputGroup>
        <Form.Label>Content</Form.Label>
        <InputGroup className="mb-3">
          
          <Form.Control type="text" name="content" as="textarea" rows={10} defaultValue={editPortfolio.content} onChange={handleChange} />
        </InputGroup>
        <Form.Label>Content</Form.Label>
        <InputGroup className="mb-3">
          
          <Form.Control type="text" name="url" defaultValue={editPortfolio.url} onChange={handleChange} />
        </InputGroup>
        <Button type="submit" variant="outline-primary">Submit</Button>
      </Form>
    </Container>
  );
};

export default EditPortfolio;
