FROM node:lts-slim

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY ./ ./

ENV REACT_APP_API=https://portfolio-329801-eeohtybc2q-nn.a.run.app

CMD npm run start
